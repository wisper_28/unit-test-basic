package com.afs.unittest;

import com.afs.unittest.Project.Project;
import com.afs.unittest.Project.ProjectType;
import com.afs.unittest.exception.UnexpectedProjectTypeException;
import com.afs.unittest.expense.ExpenseType;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.function.Executable;

import static org.junit.jupiter.api.Assertions.assertEquals;
import static org.junit.jupiter.api.Assertions.assertThrows;

class ExpenseServiceTest {
    @Test
    void should_return_internal_expense_type_when_getExpenseCodeByProject_given_internal_project() {
        // given
        Project internalProject = new Project(ProjectType.INTERNAL, "internalProject");
        ExpenseService expenseService = new ExpenseService();
        // when
        ExpenseType expenseCodeByProject = expenseService.getExpenseCodeByProject(internalProject);
        // then
        assertEquals(ExpenseType.INTERNAL_PROJECT_EXPENSE, expenseCodeByProject);
    }

    @Test
    void should_return_expense_type_A_when_getExpenseCodeByProject_given_project_is_external_and_name_is_project_A() {
        // given
        Project projectA = new Project(ProjectType.EXTERNAL, "Project A");
        ExpenseService expenseService = new ExpenseService();
        // when
        ExpenseType expenseType = expenseService.getExpenseCodeByProject(projectA);
        // then
        assertEquals(ExpenseType.EXPENSE_TYPE_A, expenseType);
    }

    @Test
    void should_return_expense_type_B_when_getExpenseCodeByProject_given_project_is_external_and_name_is_project_B() {
        // given
        Project projectB = new Project(ProjectType.EXTERNAL, "Project B");
        ExpenseService expenseService = new ExpenseService();
        // when
        ExpenseType expenseType = expenseService.getExpenseCodeByProject(projectB);
        // then
        assertEquals(ExpenseType.EXPENSE_TYPE_B, expenseType);
    }

    @Test
    void should_return_other_expense_type_when_getExpenseCodeByProject_given_project_is_external_and_has_other_name() {
        // given
        Project projectC = new Project(ProjectType.EXTERNAL, "Project C");
        ExpenseService expenseService = new ExpenseService();
        // when
        ExpenseType expenseType = expenseService.getExpenseCodeByProject(projectC);
        // then
        assertEquals(ExpenseType.OTHER_EXPENSE, expenseType);
    }

    @Test
    void should_throw_unexpected_project_exception_when_getExpenseCodeByProject_given_project_is_invalid() {
        // given
        Project projectU = new Project(ProjectType.UNEXPECTED_PROJECT_TYPE, "Project U");
        ExpenseService expenseService = new ExpenseService();
        // when
        Executable executable = () -> expenseService.getExpenseCodeByProject(projectU);
        // then
        assertThrows(UnexpectedProjectTypeException.class, executable);
//        assertThrows(UnexpectedProjectTypeException.class, () -> expenseService.getExpenseCodeByProject(projectU));
    }
}